# Vanilla Jest, testing example project

[![Using yarn](https://img.shields.io/badge/using-yarn-blue.svg)](https://yarnpkg.com/)

This is a basic project vanilla with jest and babel, configurations and examples.

## Table of contents
  - [Requirements](#requirements)
  - [Getting Started](#getting-started)
    - [setup](#setup)
  - [Contributors](#contributors)

## Requirements

You will need a Javascript package manager tool to execute the available commands and to install all the
required dependencies. Try [yarn](https://classic.yarnpkg.com/en/docs/install/).

## Getting Started

### setup

1. Install required packages
   ```sh
   yarn install
   ```
2. Write your feature code in `./src` folder

3. Run tests
   ```sh
   yarn test
   ```
3. Run tests coverage
   ```sh
   yarn test --coverage
   ```

## Contributors

- [Christopher Troc](mailto:christopher.troc@globant.com) (Frontend Engineer, Web UI developer)