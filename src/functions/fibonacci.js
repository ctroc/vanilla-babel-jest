/**
 * Fibonacci function
 * @param {number} - n
 * @return {number} - fibonacci secuence
 */
const fibonacci = (n) => {
    const partialFibonacci = (n) => n === 1 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);        
    return n === 0 ? 0 : partialFibonacci(n);
};

export default fibonacci;
