import { sum } from '../functions/sum';

test('should sum two numbers', () => {
    // Arrange
    const a = 1;
    const b = 1;
    const expected = 2;

    //Act
    const result = sum(a,b);

    //Assert
    expect(result).toBe(expected);
});