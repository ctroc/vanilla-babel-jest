import fibonacci from '../functions/fibonacci';

describe('Fibonacci testing suite', () => {
    test('return one if receive number two', () => {
        // Arrange
        const number = 2;
        const expected = 1;
        // Act
        const result = fibonacci(number);
        // Assert
        expect(result).toBe(expected);
    });

    test('returns two if receive number three', () => {
        // Arrange
        const number = 3;
        const expected = 2;
        // Act
        const result = fibonacci(number);
        // Assert 
        expect(result).toBe(expected);
    });

    test('returns three if receive number four',() => { 
         // Arrange
         const number = 4;
         const expected = 3;
         // Act
         const result = fibonacci(number);
         // Assert 
         expect(result).toBe(expected);
    });
});